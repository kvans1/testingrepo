'''

Kyle Van Schaik
ICS4U
kvans1_Dictionary02.py
Write a function histogram() that takes a string and builds a frequency listing of the characters contained in it.
Represent the frequency listing as a Python dictionary and print the in sorted order on 'key'.
Strings will be entered as lowercase.

'''
def histogram():
    UserInput=input('please enter a sentence:')
    characterList=[]
    characterDictionary={}
    #print (len(UserInput))
    for x in range(len(UserInput)):
        try:
            characterDictionary[UserInput[x]]+=1
        except KeyError:
            characterDictionary[UserInput[x]]=1
    print(characterDictionary)
histogram()
